#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.

set -x

send_back_to_the_future() {
	# stop ntp
	service ntp stop || true
	# disable systemd date services (and don't fail if systemd ain't running)
	systemctl disable systemd-timesyncd || true
	systemctl disable systemd-timedated || true
	systemctl disable ntp.service || true
	systemctl stop systemd-timesyncd || true
	systemctl stop systemd-timedated || true
	systemctl stop ntp.service || true
	# set correct date
	if [ -z "$1" ] ; then
		ntpdate -b de.pool.ntp.org
	else
		ntpdate -b $1
	fi
	# set fake date
	date --set="+398 days +6 hours + 23 minutes"
	# modify MESSAGE for notification
	MESSAGE="$MESSAGE in the future"
}

put_schroots_on_tmpfs() {
	# keep schroot sessions on tmpfs
	[ -L /var/lib/schroot ] || echo "$(date -u) - /var/lib/schroot is not a link (to /srv/workspace/varlibschroot/) as it should, please fix manually"
	mkdir -p /srv/workspace/varlibschroot
	cd /srv/workspace/varlibschroot || exit 1
	mkdir -p mount session union unpack
	mkdir -p union/overlay union/underlay
}

fixup_shm() {
	# this is always harmless
	chmod 1777 /dev/shm
}

fixup_pbuilder_lockfiles() {
	rm -f /var/cache/pbuilder/*.tgz.tmp
}

###
### main
###
MESSAGE="$(hostname -f) rebooted"

#
# fixup /(dev|run)/shm if needed
#
fixup_shm

#
# configure iptables to drop incoming UDP packets
#
iptables -I INPUT -p udp --dport 111 -j DROP

#
# put schroots on tmpfs for non debian hosts
#
case $(hostname) in
	osuosl168*)	put_schroots_on_tmpfs ;;
	osuosl169*)	put_schroots_on_tmpfs ;;
	osuosl170*)	put_schroots_on_tmpfs ;;
	osuosl171*)	put_schroots_on_tmpfs ;;
	osuosl172*)	put_schroots_on_tmpfs ;;
	*)			;;
esac

#
# fixup pbuilder lockfiles if needed
#
fixup_pbuilder_lockfiles

#
# notify jenkins reboots on irc
#
if [ "$(hostname)" = "jenkins" ] ; then
	for channel in debian-qa reproducible-builds ; do
		kgb-client --conf /srv/jenkins/kgb/$channel.conf --relay-msg "$MESSAGE"
	done
fi

#
# run some hosts in the future
#
case $(hostname) in
	codethink9*)		send_back_to_the_future ;;
	codethink11*)		send_back_to_the_future ;;
	codethink13*)		send_back_to_the_future ;;
	codethink15*)		send_back_to_the_future ;;
	ionos5*)		send_back_to_the_future ;;
	ionos6*)		send_back_to_the_future ;;
	ionos15*)		send_back_to_the_future ;;
	ionos16*)		send_back_to_the_future ;;
	osuosl170*)	send_back_to_the_future time.osuosl.org;;
	osuosl172*)	send_back_to_the_future time.osuosl.org;;
	*)			;;
esac

#
# notify about reboots
#
echo "$(date -u) - system was rebooted." | mail -s "$MESSAGE" root

exit 0
