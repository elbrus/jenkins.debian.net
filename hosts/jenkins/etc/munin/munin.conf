# Example configuration file for Munin, generated by 'make build'

# The next three variables specifies where the location of the RRD
# databases, the HTML output, logs and the lock/pid files.  They all
# must be writable by the user running munin-cron.  They are all
# defaulted to the values you see here.
#
#dbdir	/var/lib/munin
#htmldir /var/cache/munin/www
#logdir /var/log/munin
#rundir  /var/run/munin

# Where to look for the HTML templates
#
#tmpldir	/etc/munin/templates

# Where to look for the static www files
#
#staticdir /etc/munin/static

# temporary cgi files are here. note that it has to be writable by 
# the cgi user (usually nobody or httpd).
#
# cgitmpdir /var/lib/munin/cgi-tmp

# (Exactly one) directory to include all files from.
includedir /etc/munin/munin-conf.d

# You can choose the time reference for "DERIVE" like graphs, and show
# "per minute", "per hour" values instead of the default "per second"
#
#graph_period second

# Graphics files are generated either via cron or by a CGI process.
# See http://munin-monitoring.org/wiki/CgiHowto2 for more
# documentation.
# Since 2.0, munin-graph has been rewritten to use the cgi code. 
# It is single threaded *by design* now.
#
#graph_strategy cron

# munin-cgi-graph is invoked by the web server up to very many times at the
# same time.  This is not optimal since it results in high CPU and memory
# consumption to the degree that the system can thrash.  Again the default is
# 6.  Most likely the optimal number for max_cgi_graph_jobs is the same as
# max_graph_jobs.
#
#munin_cgi_graph_jobs 6

# If the automatic CGI url is wrong for your system override it here:
#
#cgiurl_graph /munin-cgi/munin-cgi-graph

# max_size_x and max_size_y are the max size of images in pixel.
# Default is 4000. Do not make it too large otherwise RRD might use all
# RAM to generate the images.
# 
#max_size_x 4000
#max_size_y 4000

# HTML files are normally generated by munin-html, no matter if the
# files are used or not. You can change this to on-demand generation
# by following the instructions in http://munin-monitoring.org/wiki/CgiHowto2
# 
# Notes: 
# - moving to CGI for HTML means you cannot have graph generated by cron.
# - cgi html has some bugs, mostly you still have to launch munin-html by hand
# 
#html_strategy cron

# munin-update runs in parallel.
#
# The default max number of processes is 16, and is probably ok for you.
#
# If set too high, it might hit some process/ram/filedesc limits.
# If set too low, munin-update might take more than 5 min.
#
# If you want munin-update to not be parallel set it to 0.
#
#max_processes 16

# RRD updates are per default, performed directly on the rrd files.
# To reduce IO and enable the use of the rrdcached, uncomment it and set it to
# the location of the socket that rrdcached uses.
#
#rrdcached_socket /var/run/rrdcached.sock

# Drop somejuser@fnord.comm and anotheruser@blibb.comm an email everytime
# something changes (OK -> WARNING, CRITICAL -> OK, etc)
#contact.someuser.command mail -s "Munin notification" somejuser@fnord.comm
#contact.anotheruser.command mail -s "Munin notification" anotheruser@blibb.comm
#
# For those with Nagios, the following might come in handy. In addition,
# the services must be defined in the Nagios server as well.
#contact.nagios.command /usr/bin/send_nsca nagios.host.comm -c /etc/nsca.conf
contacts me
contact.me.command mail -s "Munin notification ${var:host}" root

# a simple host tree
[jenkins.debian.net]
    address 127.0.0.1
    use_node_name yes
    cpu.graph_args --base 1000 -r --lower-limit 0	# also graph values about current limit (eg from when the machine had more cores)
    df._srv_workspace.warning 75
    df._srv_workspace.critical 85
    #df._dev_vda1.warning 92
    #df._dev_vda1.critical 98
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no
    munin_stats.update.warning 300
    munin_stats.update.critical 600

[ionos1-amd64.debian.net]
    address 78.137.99.97
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos2-i386.debian.net]
    address 46.16.73.166
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos3-amd64.debian.net]
    address 185.48.119.12
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos5-amd64.debian.net]
    address 85.184.249.130
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos6-i386.debian.net]
    address 213.244.192.14
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos7-amd64.debian.net]
    address 157.97.110.83
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos9-amd64.debian.net]
    address 46.16.78.222
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos10-amd64.debian.net]
    address 85.184.249.68
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos11-amd64.debian.net]
    address 78.137.101.155
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos12-i386.debian.net]
    address 46.16.73.168
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos15-amd64.debian.net]
    address 157.97.110.46
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ionos16-i386.debian.net]
    address 85.184.249.230
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vdb.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.vdb.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[codethink9-arm64.debian.net]
    address ssh://jenkins@codethink9-arm64.debian.net:10109/bin/nc localhost 4949
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[codethink10-arm64.debian.net]
    address ssh://jenkins@codethink10-arm64.debian.net:10110/bin/nc localhost 4949
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[codethink11-arm64.debian.net]
    address ssh://jenkins@codethink11-arm64.debian.net:10111/bin/nc localhost 4949
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[codethink12-arm64.debian.net]
    address ssh://jenkins@codethink12-arm64.debian.net:10112/bin/nc localhost 4949
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[codethink13-arm64.debian.net]
    address ssh://jenkins@codethink13-arm64.debian.net:10113/bin/nc localhost 4949
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[codethink14-arm64.debian.net]
    address ssh://jenkins@codethink14-arm64.debian.net:10114/bin/nc localhost 4949
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[codethink15-arm64.debian.net]
    address ssh://jenkins@codethink15-arm64.debian.net:10115/bin/nc localhost 4949
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[codethink16-arm64.debian.net]
    address ssh://jenkins@codethink16-arm64.debian.net:10116/bin/nc localhost 4949
    use_node_name yes
    df._srv_workspace.warning 90
    df._srv_workspace.critical 95
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[bbx15-armhf-rb.debian.net]
    address ssh://jenkins@bbx15-armhf-rb.debian.net:2242/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[wbq0-armhf-rb.debian.net]
    address ssh://jenkins@wbq0-armhf-rb.debian.net:2225/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[cbxi4a-armhf-rb.debian.net]
    address ssh://jenkins@cbxi4a-armhf-rb.debian.net:2239/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[cbxi4b-armhf-rb.debian.net]
    address ssh://jenkins@cbxi4b-armhf-rb.debian.net:2240/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[cbxi4pro0-armhf-rb.debian.net]
    address ssh://jenkins@cbxi4pro0-armhf-rb.debian.net:2226/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[odxu4a-armhf-rb.debian.net]
    address ssh://jenkins@odxu4a-armhf-rb.debian.net:2229/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[odxu4b-armhf-rb.debian.net]
    address ssh://jenkins@odxu4b-armhf-rb.debian.net:2232/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[jtk1a-armhf-rb.debian.net]
    address ssh://jenkins@jtk1a-armhf-rb.debian.net:2246/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[jtk1b-armhf-rb.debian.net]
    address ssh://jenkins@jtk1b-armhf-rb.debian.net:2252/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[jtx1a-armhf-rb.debian.net]
    address ssh://jenkins@jtx1a-armhf-rb.debian.net:2249/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[jtx1b-armhf-rb.debian.net]
    address ssh://jenkins@jtx1b-armhf-rb.debian.net:2253/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[jtx1c-armhf-rb.debian.net]
    address ssh://jenkins@jtx1c-armhf-rb.debian.net:2254/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[opi2a-armhf-rb.debian.net]
    address ssh://jenkins@opi2a-armhf-rb.debian.net:2236/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ff2a-armhf-rb.debian.net]
    address ssh://jenkins@ff2a-armhf-rb.debian.net:2234/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ff2b-armhf-rb.debian.net]
    address ssh://jenkins@ff2b-armhf-rb.debian.net:2237/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ff4a-armhf-rb.debian.net]
    address ssh://jenkins@ff4a-armhf-rb.debian.net:2241/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[ff64a-armhf-rb.debian.net]
    address ssh://jenkins@ff64a-armhf-rb.debian.net:2250/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[odu3a-armhf-rb.debian.net]
    address ssh://jenkins@odu3a-armhf-rb.debian.net:2243/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[cb3a-armhf-rb.debian.net]
    address ssh://jenkins@cb3a-armhf-rb.debian.net:2244/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[p64b-armhf-rb.debian.net]
    address ssh://jenkins@p64b-armhf-rb.debian.net:2247/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[p64c-armhf-rb.debian.net]
    address ssh://jenkins@p64c-armhf-rb.debian.net:2248/bin/nc localhost 4949
    use_node_name yes
    # slow usb disk
    diskstats_latency.sda.avgwrwait.warning 0:100
    diskstats_latency.sda.avgrdwait.warning 0:77
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[opi2c-armhf-rb.debian.net]
    address ssh://jenkins@opi2c-armhf-rb.debian.net:2245/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[osuosl167-amd64.debian.net]
    address ssh://jenkins@osuosl167-amd64.debian.net:45046/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[osuosl168-amd64.debian.net]
    address ssh://jenkins@osuosl168-amd64.debian.net:45047/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[osuosl169-amd64.debian.net]
    address ssh://jenkins@osuosl169-amd64.debian.net:45048/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[osuosl170-amd64.debian.net]
    address ssh://jenkins@osuosl170-amd64.debian.net:45049/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[osuosl171-amd64.debian.net]
    address ssh://jenkins@osuosl171-amd64.debian.net:45050/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[osuosl172-amd64.debian.net]
    address ssh://jenkins@osuosl172-amd64.debian.net:45051/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[osuosl173-amd64.debian.net]
    address ssh://jenkins@osuosl173-amd64.debian.net:45052/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[osuosl174-amd64.debian.net]
    address ssh://jenkins@osuosl174-amd64.debian.net:45053/bin/nc localhost 4949
    use_node_name yes
    diskstats_latency.sda.avgwrwait.warning 0:20
    diskstats_latency.sda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[freebsd-jenkins.debian.net]
    address 46.16.73.236
    use_node_name yes
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no
    diskstats_iops.graph no

[jenkins-test-vm.debian.net]
    address 46.16.73.145
    use_node_name yes
    cpu.graph_args --base 1000 -r --lower-limit 0	# also graph values about current limit (eg from when the machine had more cores)
    df._srv_workspace.warning 75
    df._srv_workspace.critical 85
    diskstats_latency.vda.avgwrwait.warning 0:20
    diskstats_latency.vda.avgrdwait.warning 0:10
    diskstats_latency.graph no
    diskstats_throughput.graph no
    diskstats_utilization.graph no

[rb-mail1.reproducible-builds.org]
    address rb-mail1.reproducible-builds.org

#
# A more complex example of a host tree
#
## First our "normal" host.
# [fii.foo.com]
#       address foo
#
## Then our other host...
# [fay.foo.com]
#       address fay
#
## Then we want totals...
# [foo.com;Totals] #Force it into the "foo.com"-domain...
#       update no   # Turn off data-fetching for this "host".
#
#   # The graph "load1". We want to see the loads of both machines...
#   # "fii=fii.foo.com:load.load" means "label=machine:graph.field"
#       load1.graph_title Loads side by side
#       load1.graph_order fii=fii.foo.com:load.load fay=fay.foo.com:load.load
#
#   # The graph "load2". Now we want them stacked on top of each other.
#       load2.graph_title Loads on top of each other
#       load2.dummy_field.stack fii=fii.foo.com:load.load fay=fay.foo.com:load.load
#       load2.dummy_field.draw AREA # We want area instead the default LINE2.
#       load2.dummy_field.label dummy # This is needed. Silly, really.
#
#   # The graph "load3". Now we want them summarised into one field
#       load3.graph_title Loads summarised
#       load3.combined_loads.sum fii.foo.com:load.load fay.foo.com:load.load
#       load3.combined_loads.label Combined loads # Must be set, as this is
#                                                 # not a dummy field!
#
## ...and on a side note, I want them listen in another order (default is
## alphabetically)
#
# # Since [foo.com] would be interpreted as a host in the domain "com", we
# # specify that this is a domain by adding a semicolon.
# [foo.com;]
#       node_order Totals fii.foo.com fay.foo.com
#
[debian.net;]
	node_order jenkins.debian.net ionos1-amd64.debian.net ionos2-i386.debian.net ionos3-amd64.debian.net ionos5-amd64.debian.net ionos6-i386.debian.net ionos7-amd64.debian.net ionos9-amd64.debian.net ionos10-amd64.debian.net ionos11-amd64.debian.net ionos12-i386.debian.net ionos15-amd64.debian.net ionos16-i386.debian.net osuosl167-amd64.debian.net osuosl168-amd64.debian.net osuosl169-amd64.debian.net osuosl170-amd64.debian.net osuosl171-amd64.debian.net osuosl172-amd64.debian.net osuosl173-amd64.debian.net osuosl174-amd64.debian.net codethink9-arm64.debian.net codethink10-arm64.debian.net codethink11-arm64.debian.net codethink12-arm64.debian.net codethink13-arm64.debian.net codethink14-arm64.debian.net codethink15-arm64.debian.net codethink16-arm64.debian.net bbx15-armhf-rb.debian.net cbxi4a-armhf-rb.debian.net cbxi4b-armhf-rb.debian.net cbxi4pro0-armhf-rb.debian.net cb3a-armhf-rb.debian.net ff2a-armhf-rb.debian.net ff2b-armhf-rb.debian.net ff4a-armhf-rb.debian.net ff64a-armhf-rb.debian.net odxu4a-armhf-rb.debian.net odxu4b-armhf-rb.debian.net odu3a-armhf-rb.debian.net jtk1a-armhf-rb.debian.net jtk1b-armhf-rb.debian.net jtx1a-armhf-rb.debian.net jtx1b-armhf-rb.debian.net jtx1c-armhf-rb.debian.net opi2a-armhf-rb.debian.net opi2c-armhf-rb.debian.net p64b-armhf-rb.debian.net p64c-armhf-rb.debian.net wbq0-armhf-rb.debian.net freebsd-jenkins.debian.net jenkins-test-vm.debian.net

