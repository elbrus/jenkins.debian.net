#!/bin/bash
# vim: set noexpandtab:

# Copyright © 2020 Holger Levsen (holger@layer-acht.org)
# released under the GPLv2
#
#
# to be run manually on jenkins in a terminal
#
# this script is partly jelle's fault, because he gave me
# https://github.com/jelly/reproduciblebuilds-display
# which made me make https://tests.reproducible-builds.org/trbo.status.html
# first and then this!

# prepare a trap for cleaning up
# using a global tempfile
PSFAX=$(mktemp --tmpdir=$TMPDIR jenkins-shell-monitor-XXXXXXX)
DISKSTATS=$(mktemp --tmpdir=$TMPDIR jenkins-shell-monitor-XXXXXXX)
cleanup_all() {
	trap - INT TERM EXIT
	echo
	rm -vf $PSFAX $DISKSTATS
	echo "Terminated, good-bye."
	exit 0
}

#
# main
#
main_loop() {
	# most data is updated every fifteen minutes
	ps fax > $PSFAX
	LSOF=$(lsof -n | wc -l)
	SCHROOT_SESSIONS=$(find /var/lib/schroot/session/ -mindepth 1 | wc -l)
	if [ $SCHROOT_SESSIONS -gt 30000 ] ; then
		SCHROOT_SESSIONS="${RED}$SCHROOT_SESSIONS${FOREGROUND_COLOR}"
	elif [ $SCHROOT_SESSIONS -gt 15000 ] ; then
		SCHROOT_SESSIONS="${YELLOW}$SCHROOT_SESSIONS${FOREGROUND_COLOR}"
	fi
	SCHROOT_MOUNTS=$(mount | grep /run/schroot| wc -l)
	SCHROOT_BASE_DIRS=$(find /schroots/ -maxdepth 1 -mindepth 1 -type d | wc -l)
	DIFFOSCOPE_PROCS=$(grep -c '/usr/bin/python3 /usr/bin/diffoscope' $PSFAX || true)
	REPRO_JOBS=$(ls ~jenkins/jobs/reproducible_* -1d | wc -l)
	JOBS_RUNNING_TOTAL=$(grep '_ /bin/bash /srv/jenkins/bin/' $PSFAX | egrep -v 'reproducible_worker.sh|reproducible_build.sh|jenkins-shell-monitor.sh|reproducible_build_archlinux_pkg.sh' | wc -l)
	if [ $JOBS_RUNNING_TOTAL -eq 0 ] ; then
		JOBS_RUNNING_TOTAL="${RED}$JOBS_RUNNING_TOTAL${FOREGROUND_COLOR}"
	elif [ $JOBS_RUNNING_TOTAL -lt 5 ] ; then
		JOBS_RUNNING_TOTAL="${YELLOW}$JOBS_RUNNING_TOTAL${FOREGROUND_COLOR}"
	fi
	JOBS_RUNNING_LOCAL=$(grep '_ /bin/bash /srv/jenkins/bin/' $PSFAX | egrep -v 'reproducible_worker.sh|reproducible_build.sh|jenkins-shell-monitor.sh|reproducible_build_archlinux_pkg.sh|jenkins_master_wrapper.sh' | wc -l)
	JOBS_RUNNING_REMOTE=$(grep -c jenkins_master_wrapper.sh $PSFAX || true)
	REPRO_WORKERS=$(grep -c reproducible_worker.sh $PSFAX || true)
	JENKINS_AGENTS=$(grep -c jenkins/agent.jar $PSFAX || true)
	NODES_TOTAL=$(find ~/nodes -mindepth 1 -type d | wc -l)
	(	printf  "%-45s %-5s %-5s %s\n" "mountpoint" "size" "avail" "usage"
		for FILESYSTEM in /dev/vda1 /var/lib/jenkins/userContent/reproducible /srv/workspace /tmp ; do
			DF_FILESYSTEM=$(df -h $FILESYSTEM | tail -1)
			FILESYSTEM_MOUNTPOINT=$(echo $DF_FILESYSTEM | awk '{ print $6 }')
			FILESYSTEM_SIZE=$(echo $DF_FILESYSTEM | awk '{ print $2 }')
			FILESYSTEM_AVAIL=$(echo $DF_FILESYSTEM | awk '{ print $4 }')
			FILESYSTEM_USAGE=$(echo $DF_FILESYSTEM | awk '{ print $5 }' | cut -d '%' -f1)
			if [ $FILESYSTEM_USAGE -gt 95 ] ; then
				FILESYSTEM_USAGE="${RED}$FILESYSTEM_USAGE%${FOREGROUND_COLOR}"
				FILESYSTEM_AVAIL="${RED}$FILESYSTEM_AVAIL${FOREGROUND_COLOR}"
				WIDTH=16
			elif [ $FILESYSTEM_USAGE -gt 90 ] ; then
				FILESYSTEM_USAGE="${YELLOW}$FILESYSTEM_USAGE%${FOREGROUND_COLOR}"
				FILESYSTEM_AVAIL="${YELLOW}$FILESYSTEM_AVAIL${FOREGROUND_COLOR}"
				WIDTH=16
			elif [ $FILESYSTEM_USAGE -gt 80 ] ; then
				FILESYSTEM_USAGE="${GREEN}$FILESYSTEM_USAGE%${FOREGROUND_COLOR}"
				FILESYSTEM_AVAIL="${GREEN}$FILESYSTEM_AVAIL${FOREGROUND_COLOR}"
				WIDTH=16
			else
				FILESYSTEM_USAGE="$FILESYSTEM_USAGE%"
				WIDTH=5
			fi
			printf  "%-45s %-5s %-${WIDTH}s %s\n" $FILESYSTEM_MOUNTPOINT $FILESYSTEM_SIZE $FILESYSTEM_AVAIL $FILESYSTEM_USAGE
		done
	) > $DISKSTATS
	SOME_AGE="$(date -u)"
	for i in $SEQ1 ; do
		clear
		LOAD="$(uptime | rev | cut -d ',' -f1-3 | rev | cut -d ':' -f2-|xargs echo)"
		INTEGER_LOAD=$(echo $LOAD | cut -d '.' -f1)
		# based on 23 CPUs
		if [ $INTEGER_LOAD -gt 69 ] ; then
			LOAD="${RED}$LOAD${FOREGROUND_COLOR}"
		elif [ $INTEGER_LOAD -gt 42 ] ; then
			LOAD="${YELLOW}$LOAD${FOREGROUND_COLOR}"
		fi
		NODES_OFFLINE="$(grep -v ^# ~jenkins/offline_nodes | grep -c debian)"
		NODES_GIT_OFFLINE="$(grep -v ^# ~jenkins-adm/jenkins.debian.net/jenkins-home/offline_nodes | grep -c debian)"
		if [ "$NODES_OFFLINE" != "$NODES_GIT_OFFLINE" ] ; then
			NODES_OFFLINE="${YELLOW}$NODES_OFFLINE${FOREGROUND_COLOR}"
		fi
		SYSTEMCTLSTATUS=$(systemctl status|head -4|tail -3)
		SYS_STATE=$(echo "$SYSTEMCTLSTATUS" | grep State | cut -d ':' -f2- | xargs echo)
		if [ "$SYS_STATE" != "running" ] ; then
			SYS_STATE="${YELLOW}$SYS_STATE${FOREGROUND_COLOR}"
		fi
		SYS_JOBS=$(echo "$SYSTEMCTLSTATUS" | grep Jobs | cut -d ':' -f2- | xargs echo)
		if [ "$SYS_JOBS" != "0 queued" ] ; then
			SYS_JOBS="${YELLOW}$SYS_JOBS${FOREGROUND_COLOR}"
		fi
		SYS_FAILED=$(echo "$SYSTEMCTLSTATUS" | grep Failed | cut -d ':' -f2- | xargs echo)
		if [ "$SYS_FAILED" != "0 units" ] ; then
			SYS_FAILED="${YELLOW}$SYS_FAILED${FOREGROUND_COLOR}"
		fi
		echo "=== jenkins/trbo shell monitor ==="
		echo
		echo "relevant lines from ~jenkins/offline_nodes:"
		grep -A 2342 Also ~jenkins/offline_nodes | tac | sed -e '/# none atm/,+1d' | tac | sed '/^$/d'
		# ^^^ this line explained:
		# we grep for everything after the string 'Also" because we know the document and this will never change... m( :)
		# tac is reverse cat. so doing it twice allows us to use sed backwards
		# the first sed removes the matching line and the following (which due to tac is the previous one)
		# the second sed command removes empty lines
		echo
		for j in $SEQ2 ; do
			echo -n "#"
		done
		echo
		figlet $(cat ~/userContent/reproducible/trbo.status) 
		echo " ^^^ trbo status, see https://tests.reproducible-builds.org/trbo.status.html"
		echo
		echo    "uptime:                                       $(uptime -p | cut -d ' ' -f2-)"
		echo -e "load averages:                                $LOAD"
		echo -e "systemctl status summary:                     $SYS_STATE"
		echo -e "systemctl jobs:                               $SYS_JOBS"
		echo -e "systemctl failed:                             $SYS_FAILED"
		echo    "logged in user sessions:                      $(uptime | rev |cut -d ',' -f1-4 | rev | cut -d ',' -f1 | sed "s#users##" | xargs echo)"
		echo    "logged in users:                              $(w -h | awk '{print $1}' | sort -u | xargs echo)"
		echo    "number of open files:                         $LSOF"
		echo -e "schroot: (sessions / mounts / base dirs)      $SCHROOT_SESSIONS / $SCHROOT_MOUNTS / $SCHROOT_BASE_DIRS"
		echo    "diffoscope processes:                         $DIFFOSCOPE_PROCS"
		echo    "configured r-b jobs:                          $REPRO_JOBS"
		echo -e "running jenkins jobs: (total/local/remote)    $JOBS_RUNNING_TOTAL / $JOBS_RUNNING_LOCAL / $JOBS_RUNNING_REMOTE"
		echo    "running debian r-b workers:                   $REPRO_WORKERS"
		echo    "running jenkings agents/nodes:                $JENKINS_AGENTS"
		echo -e "nodes: (total/auto-offline/offline in git)    $NODES_TOTAL / $NODES_OFFLINE / $NODES_GIT_OFFLINE"
		echo
		cat $DISKSTATS
		free -h | cut -b1-47
		echo
		echo "some information was gathered at $SOME_AGE. (updated every 15min)"
		echo "some other information at around $(date -u). (updated every 90sec)"
		for j in $SEQ2 ; do
			echo -n "."
			sleep 1.5
		done
	done
}

# main init
export LANG=C
trap cleanup_all INT TERM EXIT
# initialize internal loop variables
SEQ1=$(seq 1 10)	# outer loop
SEQ2=$(seq 1 60)	# inner loop
# static colors
FOREGROUND_COLOR=$(tput sgr0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
# main loop
while true ; do
	main_loop
done
