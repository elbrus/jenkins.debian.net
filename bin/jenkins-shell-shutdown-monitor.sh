#!/bin/bash
# vim: set noexpandtab:

# Copyright © 2020 Holger Levsen (holger@layer-acht.org)
# released under the GPLv2
#
#
# to be run manually on jenkins in a terminal
#
# useful to watch all remaining running jobs when bringing jenkins into
# shutdown mode and waiting for the jobs to finish.
# (so one can watch in shell instead of a webbrowser which uses a lot
# less cpu...)

TIMESPAN=23 

echo
echo "$0 is only really useful to run when jenkins (the app) is in shutdown-mode."
echo
echo "Now showing all logfiles with activity in the last $TIMESPAN minutes. Please hang on, we will be ready in a moment..."
echo
sleep 1

cd ~jenkins
MMINOPTS="-mmin 0"
for i in $(seq 1 $TIMESPAN) ; do
	MMINOPTS="$MMINOPTS -o -mmin $i"
done
find jobs/*/builds/* -name log \( $MMINOPTS \) 2>/dev/null | xargs -r tail -f
